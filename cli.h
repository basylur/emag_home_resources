/*
 * uart2_cli.h
 *
 *  Created on: Sep 8, 2016
 *      Author: Andrei
 */

#ifndef SOURCES_CLI_H_
#define SOURCES_CLI_H_

#include "types.h"
#include "uart.h"
#include "mcg.h"
#include "compileOptions.h"

void Uart0CLIinit(void);
void UART0_IRQHandler(void);
void UART0_CLI_Process(void);
void RS232TXData(uint8 *data,uint16 len);
void RS232TXStr(uint8 *string);
uint8 CliTransmitDone(void);

extern Uart_Variables_type Uart0CLI;
extern uint8 TmpPrintBuff[64];
extern uint8 DebugVar;

void parser(vuint8 *CmdBuffer);

#endif /* SOURCES_DRIVERS_UART_UART2_CLI_H_ */
