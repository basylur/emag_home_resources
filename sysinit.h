/*
 * sysinit.h
 *
 *  Created on: Sep 7, 2016
 *      Author: Andrei
 */

#ifndef SOURCES_SYSINIT_H_
#define SOURCES_SYSINIT_H_

#include "types.h"

extern uint16 ResetSource;

#define RESET_SOURCE_NA           0x0000
#define RESET_SOURCE_MDM_AP       0x0800
#define RESET_SOURCE_SW           0x0400
#define RESET_SOURCE_CORE_LOCK_UP 0x0200
#define RESET_SOURCE_POR          0x0082
#define RESET_SOURCE_LP_WU_RESET  0x0041
#define RESET_SOURCE_RESET_PIN    0x0040
#define RESET_SOURCE_COP          0x0020
#define RESET_SOURCE_LVD          0x0002
#define RESET_SOURCE_LP_WU        0x0001

void sysinit(void);
void FeedIntWatchdog(void);

#endif /* SOURCES_SYSINIT_H_ */
