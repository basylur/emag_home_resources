/*
 * types.h
 *
 *  Created on: Jul 26, 2012
 *      Author: Andrei
 */

#ifndef TYPES_H_
#define TYPES_H_

#include "MKL26Z4.h"

/* defines for peripheral configuration structures                            */ 
#define SET(mask)       mask
#define CLR(mask)       0

#ifdef  NULL
#undef  NULL
#endif
#define NULL  (0)

/***********************************************************************/
/*
 * The basic data types
 */
typedef unsigned char		uint8;  /*  8 bits */
typedef unsigned short int	uint16; /* 16 bits */
typedef unsigned long int	uint32; /* 32 bits */

typedef unsigned long int *	uint32_ptr; /* 32 bits */

typedef char			    int8;   /*  8 bits */
typedef short int	    int16;  /* 16 bits */
typedef int		        int32;  /* 32 bits */

typedef volatile int8		vint8;  /*  8 bits */
typedef volatile int16	vint16; /* 16 bits */
typedef volatile int32	vint32; /* 32 bits */

typedef volatile uint8	vuint8;  /*  8 bits */
typedef volatile uint16	vuint16; /* 16 bits */
typedef volatile uint32	vuint32; /* 32 bits */

#endif /* TYPES_H_ */
