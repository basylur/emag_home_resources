/*
 * pit.c
 *
 *  Created on: Sep 12, 2016
 *      Author: Andrei
 */
#include "MKL26Z4.h"
#include "timers.h"
#include "mcg.h"
#include "sysinit.h"

typedef struct pit_cal{
uint32 LDVAL;
uint8  TCTRL;
uint8  TFLG;
} tPit_Cal ;

vuint32 TimerTicks;
vuint16 ticks=0,seconds=0,minutes=0,hours=0;
/**********************************************************************************
Function Name:  PITtimerSetup
Description:    Sets up a pit channel from a pointer to a settings structure previously populated
                with the correct values
Parameters:     None
Return value:   None
***********************************************************************************/
void PITtimerSetup(tPit_Cal* settings,uint8 pitTimerCH){
if(pitTimerCH>1) return;
PIT_LDVAL_REG(PIT_BASE_PTR,pitTimerCH) = settings->LDVAL;
PIT_TCTRL_REG(PIT_BASE_PTR,pitTimerCH)= settings->TCTRL;
PIT_TFLG_REG(PIT_BASE_PTR,pitTimerCH) = settings->TFLG;
}

/**********************************************************************************
Function Name:  PITInit
Description:    initializes  pit channel 0 in order to be used as an uptime clock and pit ch 1
                as a programmable delay block
Parameters:     None
Return value:   None
***********************************************************************************/
void PITInit(void){
tPit_Cal pitChsettings;
//enable the module clock
SIM_SCGC6 |=  SIM_SCGC6_PIT_MASK;
// Configure PIT
PIT_MCR = ~(PIT_MCR_FRZ_MASK |PIT_MCR_MDIS_MASK);//enable the module, allow the timers to run in debug mode
//Configure channel 0
//configure timer 0 to overflow @ 10ms in order to use it as an uptime clock
pitChsettings.LDVAL = ms_10_at_busClock;//set overflow at 10 ms
pitChsettings.TFLG = PIT_TFLG_TIF_MASK;//write 1 to remove the overflow flag
pitChsettings.TCTRL = PIT_TCTRL_TIE_MASK | PIT_TCTRL_TEN_MASK;//enable the timmer and the interrupt
PITtimerSetup(&pitChsettings,0);
NVIC_EnableIRQ(PIT_IRQn);//enable the pit interrupt
//channel 1 will be used as a programmable delay block
}


/**********************************************************************************
Function Name:
Description:
Parameters:     None
Return value:   None
***********************************************************************************/
void PIT_Change_Clock(void){
tPit_Cal pitChsettings;
NVIC_DisableIRQ(PIT_IRQn);//disable the pit interrupt
// Configure PIT
PIT_MCR = ~(PIT_MCR_FRZ_MASK |PIT_MCR_MDIS_MASK);//enable the module, allow the timers to run in debug mode
//Configure channel 0
//configure timer 0 to overflow @ 10ms in order to use it as an uptime clock
pitChsettings.LDVAL = ms_10_at_busClock;//set overflow at 10 ms
pitChsettings.TFLG = PIT_TFLG_TIF_MASK;//write 1 to remove the overflow flag
pitChsettings.TCTRL = PIT_TCTRL_TIE_MASK | PIT_TCTRL_TEN_MASK;//enable the timmer and the interrupt
PITtimerSetup(&pitChsettings,0);
NVIC_EnableIRQ(PIT_IRQn);//enable the pit interrupt
//channel 1 will be used as a programmable delay block
}


/**********************************************************************************
Function Name:  PIT0_IRQ
Description:    interrupt service routine for channel 0 of PIT
Parameters:   None
Return value:   None
***********************************************************************************/
void PIT_IRQHandler(void){
PIT_TFLG0 = PIT_TFLG_TIF_MASK;//remove the flag
TimerTicks++;
if((TimerTicks%100)==0){
  seconds++;
  if(seconds==60) {
    seconds=0;
    minutes++;
    if(minutes==60){
      hours++;
      minutes=0;
    }
  }
}
}
/**********************************************************************************
Function Name:  StartTimer
Description:  Save the current timer in order to compute an interval
Parameters:   pointer to the variable where the timer is stored
Return value: None
***********************************************************************************/
void StartTimer(uint16 *ptr){
*ptr = (uint16)TimerTicks;
}
/**********************************************************************************
Function Name:  TimerExpired
Description:  Save the current timer in order to compute an interval
Parameters:   The timer, first configured with StartTimer function and the interval
Return value: 1 Expired, 0 not expired, 2 stopped ???
***********************************************************************************/
uint8 TimerExpired(uint16 *timer, uint16 interval){
if(((uint16)(TimerTicks - *timer)) >= interval){
  *timer = (uint16)TimerTicks;
  return 1;
}
return 0;
}
