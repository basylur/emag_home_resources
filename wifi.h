/*
 * uart2_cli.h
 *
 *  Created on: Sep 8, 2016
 *      Author: Andrei
 */

#ifndef SOURCES_WIFI_H_
#define SOURCES_WIFI_H_

#include "types.h"
#include "uart.h"
#include "mcg.h"
#include "compileOptions.h"

void Uart2CLIinit(void);
void UART2_IRQHandler(void);
void UART2_CLI_Process(void);
void WifiTXData(uint8 *data,uint16 len);
void WifiTXStr(uint8 *string);
uint8 WifiTransmitDone(void);

uint8 newAtCmd(uint8 *cmd,uint8 longTimeout);

extern Uart_Variables_type Uart2CLI;
extern uint8 WifiTmpPrintBuff[64];

void WifiParser(vuint8 *CmdBuffer);

void ATCommandInit(void);
void gsmAtProcess(void);

#define MAX_AT_CODE_LEN  10
#define MAX_PAYLOAD_LEN  100
#define MAX_AT_MSG_LEN   (MAX_AT_CODE_LEN+MAX_PAYLOAD_LEN)
typedef struct at_msg_t{
  uint8 state;
  uint8 flags;
  uint8 data[MAX_AT_MSG_LEN];
} AT_msg_type;

#define AT_MSG_FLAGS_LONG_TIMEOUT 0x01

#define RECEIVE_TIMEOUT 30


void ManagementProcess(void);

uint8 SendTCPdata(void);
extern const uint8 HttpResponse[] ;

#endif /* SOURCES_WIFI_H_ */
