/*
 * File:		uart.h
 * Purpose:     Provide common ColdFire UART routines for polled serial IO
 *
 * Notes:
 */

#ifndef __UART_H__
#define __UART_H__

#include "types.h"

//RS232_CLI_Process_State
#define CLI_FREE      0x00
#define CLI_NEW_DATA  0x01
#define CLI_PRINT     0x02
#define CLI_XMODEM    0x03
#define CLI_IPMB_REQ  0x04
#define CLI_DATA_SENT 0x05
#define CLI_DATA_ACK  0x06
#define CLI_TFTP_REQ  0x07

// uart flags bits
#define REPETLASTCOMMAND  0x0001
#define BUFFEREMPTY       0x0002            // DATA SENT for telnet
#define NEWDATA           0x0004
#define ESCAPECODE        0x0008
#define BACKSPACE         0x0010
#define RX_TIMER_STARTED  0x0020
#define CR_DETECTED       0x0040
#define GSM_UPDATE        0x0080
#define RECEIVER_OVERRUN  0x0100
#define RX_BUFFER_FREEZE  0x0200


void uart_init (UART_MemMapPtr uartch, uint32 sysclk, uint32 baud);

typedef struct Uart_Variables_t{
	uint8 flags;
	vuint8 *TXBuffer;
	vuint8 *RXBuffer;
	uint16 TXBuffSize;
	uint16 RXBuffSize;
	uint16 TXCharCount;
	uint16 TXWriteIndex;
	uint16 TXReadIndex;
	uint16 RXWriteIndex;
	uint16 RXtimer;
	void* uartChannel;
}Uart_Variables_type;

void SendSerialStr(Uart_Variables_type *uart,uint8 *string);
void SendSerialData(Uart_Variables_type *uart,uint8 *data,uint16 len);
void Default_Uart_RX_TX_Irq(Uart_Variables_type *uart);


#endif /* __UART_H__ */
