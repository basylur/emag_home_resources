/*
 * compile_options.h
 *
 *  Created on: Sep 8, 2017
 *      Author: Andrei
 */

#ifndef SOURCES_COMPILE_OPTIONS_H_
#define SOURCES_COMPILE_OPTIONS_H_


#define CLI_BAUD_RATE     19200
#define ESP_03_BAUD_RATE  9600


#endif /* SOURCES_COMPILE_OPTIONS_H_ */
