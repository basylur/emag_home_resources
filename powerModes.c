/*
 * powerModes.c
 *
 *  Created on: Sep 15, 2016
 *      Author: Andrei
 */

#include "types.h"
#include "MKL26Z4.h"
#include "powerModes.h"


/**********************************************************************************
Function Name: initPowerModes
Description:  sets the write once registers (SMC_PMPROT & SMC_)
Parameters: None
Return value:   None
***********************************************************************************/
void initPowerModes(void){
//allow very low power modes: vlpr,vlpw,vlps
SMC_PMPROT = SMC_PMPROT_AVLP_MASK;
}
/**********************************************************************************
Function Name: getPowerMode
Description:  returns the current power mode
Parameters: None
Return value:   None
***********************************************************************************/
uint8 getPowerMode(void){
return SMC_PMSTAT;
}

