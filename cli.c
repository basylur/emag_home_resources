/*
 * UART0_cli.c
 *
 *  Created on: Sep 8, 2016
 *      Author: Andrei
 */


#include "cli.h"
#include "util.h"
#include "sysinit.h"
#include "mcg.h"
#include "version.h"
#include "powerModes.h"
#include "timers.h"
#include "wifi.h"
#include "IO_Process.h"


uint8 TmpPrintBuff[64];
uint8 DebugVar;

void parser(vuint8 *CmdBuffer);

//size used as AND mask
#define UART0_RX_BUFF_SIZE 0xFF
#define UART0_TX_BUFF_SIZE 0xFF

uint8 aux8;
uint16 aux16;

Uart_Variables_type Uart0CLI;
vuint8 Uart0_TXBuffer[UART0_TX_BUFF_SIZE+1],Uart0_RXBuffer[UART0_RX_BUFF_SIZE+1];

uint8 Uart0_CLI_Process_State =0;



/**********************************************************************************
Function Name: UART0 CLi init
Description:
Parameters:   None
Return value:   None
 ***********************************************************************************/
void Uart0CLIinit(void){
uint32 aux32=0;
Uart0CLI.TXBuffer = (uint8*)Uart0_TXBuffer;
Uart0CLI.RXBuffer = (uint8*)Uart0_RXBuffer;
Uart0CLI.TXBuffSize = UART0_TX_BUFF_SIZE;
Uart0CLI.RXBuffSize = UART0_RX_BUFF_SIZE;
Uart0CLI.TXCharCount = 0;
Uart0CLI.TXWriteIndex = 0;
Uart0CLI.TXReadIndex = 0;
Uart0CLI.RXWriteIndex = 0;
Uart0CLI.uartChannel = UART0_BASE_PTR;
Uart0CLI.flags = BUFFEREMPTY;
uart_init ((UART_MemMapPtr)UART0_BASE_PTR,CoreClock,CLI_BAUD_RATE);
RS232TXStr((uint8*)"\r\n\r\n Emag Dash CLI SW V:");
inttotxt(TmpPrintBuff,MAJOR_REV);
RS232TXStr((uint8*)TmpPrintBuff);
RS232TXStr((uint8*)".");
inttotxt(TmpPrintBuff,MINOR_REV);
RS232TXStr((uint8*)TmpPrintBuff);
RS232TXStr((uint8*)" b.");
inttotxt(TmpPrintBuff,BUILD_NO);
RS232TXStr((uint8*)TmpPrintBuff);
RS232TXStr((uint8*)"\r\n%>");
while(CliTransmitDone()==0){
  //xxx
  //watchdog
}
}
/**********************************************************************************
Function Name: UART0_CLI_Process
Description:
Parameters:   None
Return value:   None
 ***********************************************************************************/
void UART0_CLI_Process(void){

if(((Uart0CLI.flags & NEWDATA)!=0) && (Uart0_CLI_Process_State == CLI_FREE)) Uart0_CLI_Process_State = CLI_NEW_DATA;

switch(Uart0_CLI_Process_State){
  case CLI_FREE:
    if((Uart0CLI.flags & REPETLASTCOMMAND)!=0){
      Uart0CLI.RXWriteIndex = strlength((const uint8*)Uart0CLI.RXBuffer);
      RS232TXStr((uint8*)Uart0CLI.RXBuffer);
      Uart0CLI.flags &=~REPETLASTCOMMAND;
    }
    break;
  case CLI_NEW_DATA:
    if (Uart0CLI.flags & ESCAPECODE) {
      Uart0CLI.flags &= ~ESCAPECODE;
      Uart0_CLI_Process_State = CLI_FREE;
    }else{
      parser(Uart0CLI.RXBuffer);
    }
    Uart0CLI.RXWriteIndex = 0;
    Uart0CLI.flags &=~ NEWDATA;
    RS232TXStr((uint8*)"\r\n%>");
    Uart0_CLI_Process_State = CLI_FREE;
    break;
  default:
    Uart0_CLI_Process_State = CLI_FREE;
    break;
  }
}
/**********************************************************************************
Function Name: RS232TXData
Description:
Parameters:   None
Return value:   None
 ***********************************************************************************/
void RS232TXData(uint8 *data,uint16 len){
  SendSerialData(&Uart0CLI,data,len);
}
/**********************************************************************************
Function Name: RS232TXStr
Description:
Parameters:   None
Return value:   None
 ***********************************************************************************/
void RS232TXStr(uint8 *string){
  SendSerialStr(&Uart0CLI,string);
}
/**********************************************************************************
Function Name: SCI5_RX_TX_Irq
Description:
Parameters:   None
Return value:   None
 ***********************************************************************************/
void UART0_IRQHandler(void){
uint8 c,XmodemChar[2];//store the received character and the string termination character in case it needs to be printed
XmodemChar[1] = 0;
//-------------------------------------TX int------------------------------
if((UART0_S1 & UART_S1_TDRE_MASK)&&(UART0_C2 & UART_C2_TIE_MASK)){//the tx int is enabled and the flag is asserted
  if(Uart0CLI.TXCharCount){                                   // send if chars are in buffer
    UART0_D = Uart0CLI.TXBuffer [Uart0CLI.TXReadIndex++];              // load tx register, inc index
    Uart0CLI.TXReadIndex = Uart0CLI.TXReadIndex & Uart0CLI.TXBuffSize;          // adjust index
    Uart0CLI.TXCharCount--;                                    // char sent, dec count
  } else {                                              // buffer empty, nothing to do
    Uart0CLI.flags |=BUFFEREMPTY;                          // set empty flag
    UART0_C2 &= ~UART_C2_TIE_MASK;                      /* Transmit Interrupt Disable (TIE = 0 ) */
  }
  return;
}
//----------------------------------------RX int----------------------------
if((UART0_S1 & UART_S1_RDRF_MASK)&&(UART0_C2 & UART_C2_RIE_MASK)){//the rx int is enabled and the flag is asserted
  c = UART0_D;// get the char
  //--------------------------------------------normal operation-----------------------------------
  if((Uart0CLI.flags & REPETLASTCOMMAND)!=0){
    c = 0;
  }
  if((Uart0CLI.flags & NEWDATA)!=0){
    if((c==ISO_ESC)||(c==155)) Uart0CLI.flags |=ESCAPECODE;
    return;
  }
  if (Uart0CLI.flags & ESCAPECODE){
    if(!((c==ISO_ESC)||(c==155))){//if esc wasn't presed before arrow up
      if (c!='[') Uart0CLI.flags &= ~ESCAPECODE;
      if((c=='A') && (Uart0CLI.RXWriteIndex==0) && (!(Uart0CLI.flags & BACKSPACE))){
        Uart0CLI.flags |= REPETLASTCOMMAND;
      }
      c=0;
    }
  }
  if((c==ISO_ESC)||(c==155)){
    Uart0CLI.flags |=ESCAPECODE;
  } else if(c==ISO_CR){
    Uart0CLI.flags |=NEWDATA;
    Uart0CLI.flags &= ~BACKSPACE;
    Uart0CLI.RXBuffer[Uart0CLI.RXWriteIndex++]=0;
  } else if(c>31 && c<127 ){
    if(Uart0CLI.RXWriteIndex<(Uart0CLI.RXBuffSize-1)){
      if(Uart0CLI.TXCharCount < Uart0CLI.TXBuffSize){
        XmodemChar[0] = c;
        RS232TXStr((uint8*)XmodemChar);
      }
      Uart0CLI.RXBuffer[Uart0CLI.RXWriteIndex++]=c;
    } else if(Uart0CLI.TXCharCount < Uart0CLI.TXBuffSize){
      XmodemChar[0] = ISO_BEEP;
      RS232TXStr((uint8*)XmodemChar);
    }
  }else if(c==ISO_BACKSPACE || c==ISO_DEL){
    if(Uart0CLI.RXWriteIndex){
      Uart0CLI.RXWriteIndex--;
      if(Uart0CLI.TXCharCount < Uart0CLI.TXBuffSize){
        XmodemChar[0] = ISO_BACKSPACE;
        RS232TXStr((uint8*)XmodemChar);
      }
      c = ISO_SPACE;
      if(Uart0CLI.TXCharCount < Uart0CLI.TXBuffSize){
        XmodemChar[0] =ISO_SPACE;
        RS232TXStr((uint8*)XmodemChar);
      }
      if(Uart0CLI.TXCharCount < Uart0CLI.TXBuffSize){
        XmodemChar[0] =ISO_BACKSPACE;
        RS232TXStr((uint8*)XmodemChar);
      }
      Uart0CLI.flags |=BACKSPACE;
    }else if(Uart0CLI.TXCharCount < Uart0CLI.TXBuffSize){
      XmodemChar[0] = ISO_BEEP;
      RS232TXStr((uint8*)XmodemChar);
    }
  }

}
}

/**********************************************************************************
Function Name: CliTransmitDone
Description:
Parameters:   None
Return value:   1 -> no more data to transmit; 0 : tehre is still data in the buffer
 ***********************************************************************************/
uint8 CliTransmitDone(void){
return (Uart0CLI.flags & BUFFEREMPTY);
}

/**********************************************************************************
Function Name:  parser
Description:  This is the Command Line Interpreter. It parses the input command
in the Command Buffer and put the answer to OutBuffer
Parameters:   uint8 *CmdBuffer, uint8 *OutBuffer,
Return value:   None
***********************************************************************************/
void parser(vuint8 *CmdBuffer){
uint16 len,i;
while(*CmdBuffer==ISO_SPACE) CmdBuffer++;

// --------------------------------version--------------------------------------------------
if((len=strcompare((uint8 *)CmdBuffer,(uint8*)"version"))){
  CmdBuffer+=len;
  RS232TXStr((uint8*)"\r\n\r\n Firmware rev.");
  inttotxt(TmpPrintBuff,MAJOR_REV);
  RS232TXStr((uint8*)TmpPrintBuff);
  RS232TXStr((uint8*)".");
  inttotxt(TmpPrintBuff,MINOR_REV);
  RS232TXStr((uint8*)TmpPrintBuff);
  RS232TXStr((uint8*)" b.");
  inttotxt(TmpPrintBuff,BUILD_NO);
  RS232TXStr((uint8*)TmpPrintBuff);
  RS232TXStr((uint8*)"\r\n");
}else if((len=strcompare((uint8 *)CmdBuffer,(uint8*)"led"))){
  CmdBuffer+=len;
  aux16 = 0;
  CmdBuffer+=strtoint((uint8*)CmdBuffer,&aux16);
  New_Led_State = aux16;
}else if((len=strcompare((uint8 *)CmdBuffer,(uint8*)"wifi"))){
  CmdBuffer+=len;
  //message for the esp-03 module
  aux16 = strlength((uint8*)CmdBuffer);
  CmdBuffer[aux16++] = ISO_CR;
  CmdBuffer[aux16++] = ISO_NL;
  CmdBuffer[aux16] = 0;
  WifiTXStr((uint8*)CmdBuffer);
  CmdBuffer+=aux16;
}else if((len=strcompare((uint8 *)CmdBuffer,(uint8*)"upload"))){
  CmdBuffer+=len;
  SendTCPdata();
  // --------------------------------power_mode--------------------------------------------------
}else if((len=strcompare((uint8 *)CmdBuffer,(uint8*)"power_mode"))){
  CmdBuffer+=len;
  if(*CmdBuffer == 0){
    aux8 = getPowerMode();
    RS232TXStr((uint8*)"\r\n Current Power mode: ");
    switch(aux8){
    case POWER_MODE_RUN:
      RS232TXStr((uint8*)"Run");
      break;
    case POWER_MODE_VLPR:
      RS232TXStr((uint8*)"VLPR");
      break;
    case POWER_MODE_VLPW:
      RS232TXStr((uint8*)"VLPW");
      break;
    case POWER_MODE_LLS:
      RS232TXStr((uint8*)"LLS");
      break;
    default:
      chartohexstr(TmpPrintBuff,aux8);
      RS232TXStr(TmpPrintBuff);

    }
    RS232TXStr((uint8*)"\r\n");
  }
// --------------------------------reboot--------------------------------------------------
}else if((len=strcompare((uint8 *)CmdBuffer,(uint8*)"reboot"))){
  CmdBuffer+=len;
  NVIC_SystemReset();
//--------------------------------------------help--------------------------------------------
}else if((len=strcompare((uint8 *)CmdBuffer,(uint8*)"help"))){
  CmdBuffer+=len;
  if(*CmdBuffer==0){
    RS232TXStr((uint8*)"\r\n\r\n List of available commands : ");
    RS232TXStr((uint8*)"\r\n* reboot");
    RS232TXStr((uint8*)"\r\n* version");
    RS232TXStr((uint8*)"\r\n");
  }
// --------------------------------bad command--------------------------------------------------
}else if(*CmdBuffer!=0) {
 bad_command:
 RS232TXStr((uint8 *)"\r\n Bad command!");
 return;
}else{//no cmd entered;only cr send
}
while(*CmdBuffer==ISO_SPACE) CmdBuffer++;
if(*CmdBuffer!=0) {
 RS232TXStr((uint8 *)"\r\n Wrong Parameters!");
 return;
}
}
