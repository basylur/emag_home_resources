/*
 * sysinit.c
 *
 *  Created on: Sep 7, 2016
 *      Author: Andrei
 */
#include "mcg.h"
#include "compileOptions.h"
#include "uart.h"
#include "cli.h"
#include "wifi.h"
#include "powerModes.h"
#include "timers.h"

void IOpinsInit (void);

/**********************************************************************************
Function: sysinit
Description:
Parameters: None
Return value:   None
***********************************************************************************/
void sysinit(void){
  initPowerModes();
  IOpinsInit();
  ClockInit();
  PITInit();
  Uart0CLIinit();//debug cli
  Uart2CLIinit();//esp-03
}


/**********************************************************************************
Function Name:IOpinsInit
Description:	enables the clocks for various ports. this have to be enabled in order
				to configure pin muxing options
Parameters: None
Return value: 	None
***********************************************************************************/
void IOpinsInit (void){
SIM_SCGC5 |= SIM_SCGC5_PORTA_MASK | SIM_SCGC5_PORTC_MASK| SIM_SCGC5_PORTD_MASK | SIM_SCGC5_PORTE_MASK;

PORTA_PCR18 = PORT_PCR_MUX(0x0);//xtal
PORTA_PCR19 = PORT_PCR_MUX(0x0);//xtal

PORTE_PCR20 =  PORT_PCR_MUX(0x4);//UART0 TX
PORTE_PCR21 =  PORT_PCR_MUX(0x4);//UART0 RX

PORTE_PCR22 =  PORT_PCR_MUX(0x4);//UART2 TX
PORTE_PCR23 =  PORT_PCR_MUX(0x4);//UART2 RX

//PORTA_PCR2 =  PORT_PCR_MUX(0x2);
//PORTA_PCR1 =  PORT_PCR_MUX(0x2);

//blue led
PORTD_PCR5 = PORT_PCR_MUX(0x1);//io

//push button
PORTC_PCR1 = PORT_PCR_MUX(0x1);//io

//led
PORTC_PCR2 = PORT_PCR_MUX(0x1);//io

}



