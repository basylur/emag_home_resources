#include "util.h"

/*-----------------------------------------------------------------------------------*/


uint16 strcomparewithoutend( uint8 *text, const uint8 *command){
uint16 len;
len=0;
while(command[len]!=0){
     if(text[len]!=command[len]) return 0;
     len++;
}
//while(text[len]==ISO_SPACE) len++;
return len;
}
/*-----------------------------------------------------------------------------------*/
uint16 strlength( const uint8 *text){
uint16 len;
len=0;
while(text[len]!=0){
     len++;
		 if(len==0xFFFF) return 0;
}
return len;
}
/*-----------------------------------------------------------------------------------*/

uint8 strcompare(uint8 *text,uint8 *command){
uint8 len;
len=0;
while(command[len]!=0){
     if(text[len]!=command[len]) return 0;
     len++;
}
if(text[len]!=0 && text[len]!=ISO_SPACE)return 0;
while(text[len]==ISO_SPACE) len++;
return len;
}
//-----------------------------------------------------------------------------------------------------------
uint16 strtoint(uint8 *str,int16 *val){
uint16 t,len;
len=0;
t=0;
*val=1;
if(str[len]==ISO_MINUS){ 
  *val=-1;
  len++;
}
while(str[len]>=ISO_0 && str[len]<=ISO_9){
   t*=10;
   t+=(str[len]-ISO_0);
   len++;
}
*val=(*val)*t;
return len;
}
//-----------------------------------------------------------------------------------------------------------
uint16 strtolong(uint8 *str,int32 *val){
uint32 t;
uint16 len;
len=0;
t=0;
*val=1;
if(str[len]==ISO_MINUS){ 
  *val=-1;
  len++;
}
while(str[len]>=ISO_0 && str[len]<=ISO_9){
   t*=10;
   t+=(str[len]-ISO_0);
   len++;
}
*val=(*val)*t;
return len;
}
//-----------------------------------------------------------------------------------------------------------
uint16 binstrtoint(uint8 *str,int16 *val){
uint16 len;
*val=0;
if(str[0]!=ISO_b) return 0;
len=1;
while(str[len]==ISO_0 || str[len]==ISO_1){
   *val = (*val)*2+(str[len]-ISO_0);
   len++;
}
return len;
}
//-----------------------------------------------------------------------------------------------------------
uint16 strtofloat(uint8 *number,float *out){
float val,k;
uint16 len,i,d,p;
int32 t;
d=1;
val=0;

len=strtolong((uint8*)number,(int32*)&t);
number+=len;
val=t;
if(*number==ISO_DOT){
     number++;
     len++;
     p=strtolong((uint8*)number,(int32*)&t);
     for(i=0;i<p; i++) d*=10;
     k=t;
     if(val<0) k=k*(-1);
     val+=(k/d);
     len+=p;
}
*out=val;
return len;
}

//-----------------------------------------------------------------------------------------------------------
void inttohexstr(uint8 *buffer,uint16 no){
uint16 i,t;
buffer[0]=ISO_0;
buffer[1]=ISO_x;
for(i=0;i<4;i++){
  t=no&(0xF000>>(i*4));
  t=t>>(12-4*i);
  if(t<10){
        buffer[i+2]=t+ISO_0;
  }else buffer[i+2]=t-10+'A';
  }
  buffer[6]=0;
}


//-----------------------------------------------------------------------------------------------------------
void chartohexstr(uint8 *buffer,uint8 no){
uint8 i,t;

buffer[0]=ISO_0;
buffer[1]=ISO_x;
for(i=0;i<2;i++){
  t=no&(0xF0>>(i*4));
  t=t>>(4-4*i);
  if(t<10){
        buffer[i+2]=t+ISO_0;
  }else buffer[i+2]=t-10+ISO_A;
  }
  buffer[4]=0;

}
//-----------------------------------------------------------------------------------------------------------
uint16 inttosizestr(uint8 *buffer,uint16 no,uint16 size){
uint16 i,t,len,digit;
uint8 tmpbuff[5];
len=0;
if (size>5) return 0;
for(i=0;i<5;i++) tmpbuff[i]=ISO_0;
do {
  t=no/10;
  digit=no-(t*10);
  no=t;
  tmpbuff[len]=digit+ISO_0;
  len++;
}while(no);
if(size==0) size=len;
if(size>len) len=size;
for(i=0;i<size;i++) buffer[i]=tmpbuff[len-i-1];
buffer[size]=0;
return size;
}
//-----------------------------------------------------------------------------------------------------------
uint16 hexstrtoint(uint8 *buffer,int16 *no){
uint16 len,t,s;
len=0;

if(buffer[0]==ISO_0 && buffer[1]==ISO_x){
  len+=2;
  s=1;
  t=0;
  while(s){
   s=0;
   if(buffer[len]>=ISO_0 && buffer[len]<=ISO_9){
       t*=16;
       t+=(buffer[len]-ISO_0);
       len++;
       s++;
   }
   if(buffer[len]>=ISO_A && buffer[len]<=ISO_F){
       t*=16;
       t+=(buffer[len]-ISO_A+10);
       len++;
       s++;
   }
   if(buffer[len]>=ISO_a && buffer[len]<=ISO_f){
       t*=16;
       t+=(buffer[len]-ISO_a+10);
       len++;
       s++;
   }
  }
  *no=t;
} else if(buffer[0]==ISO_b){ 
       len=binstrtoint(buffer,no);
     } else len=strtoint(buffer,no);
return len;
}
//-----------------------------------------------------------------------------------------------------------
uint16 hexstrtolong(uint8 *buffer,int32 *no){
uint16 len,s;
int32 t;
len=0;

if(buffer[0]==ISO_0 && buffer[1]==ISO_x){
  len+=2;
  s=1;
  t=0;
  while(s){
   s=0;
   if(buffer[len]>=ISO_0 && buffer[len]<=ISO_9){
       t*=16;
       t+=(buffer[len]-ISO_0);
       len++;
       s++;
   }
   if(buffer[len]>=ISO_A && buffer[len]<=ISO_F){
       t*=16;
       t+=(buffer[len]-ISO_A+10);
       len++;
       s++;
   }
   if(buffer[len]>=ISO_a && buffer[len]<=ISO_f){
       t*=16;
       t+=(buffer[len]-ISO_a+10);
       len++;
       s++;
   }
  }
  *no=t;
} else if(buffer[0]==ISO_b){ 
  len= binstrtoint(buffer,(int16 *)no);
} else len=strtoint(buffer,(int16 *)no);
return len;
}
//-----------------------------------------------------------------------------------------------------------

uint16 uinttobintxt(uint8 *str, uint16 no, uint8 len){
  uint8 i;
  str[0]=ISO_B;
  for(i=0;i<len;i++){
     if(no&0x01){
       str[len-i]=ISO_1;
     } else str[len-i]=ISO_0;
     no=no>>1;
  }
  str[len+1]=0;
  return len+1;
}

//-----------------------------------------------------------------------------------------------------------

uint16 inttotxt(uint8 *str, int16 no){
  uint8 i,t,j;
  i=0;
  j=0;
  if(no<0){
      *str=ISO_MINUS;
      j=1;
      no=no*(-1);
  }
  t = ISO_0 + no / 10000;
  if(t != ISO_0 ) {
    str[i+j] = t;
    i++;
  }  
  t = ISO_0 + (no / 1000) % 10;
  if(t != ISO_0 || i!=0) {
    str[i+j] = t;
    i++;
  }
  t = ISO_0 + (no / 100) % 10;
  if(t != ISO_0 || i!=0) {
    str[i+j] = t;
    i++;
  }
  t = ISO_0 + (no / 10) % 10;
  if(t != ISO_0 || i!=0) {
    str[i+j] = t;
    i++;
  }
  str[i+j] = ISO_0 + (no % 10);
  i++;
  str[i+j]=0;
  return i+j;
}

//-----------------------------------------------------------------------------------------------------------
uint8 textadd(uint8 *str, const uint8 *strtoadd){
uint16 len;
len=0;
while(strtoadd[len]!=0){
    str[len]=strtoadd[len];
    len++;
    if(len>255) break;
}
str[len]=0;
return len;
}
//-----------------------------------------------------------------------------------------------------------
void addlentext(uint8 *str, const uint8 *strtoadd, uint16 len){
uint16 i;
for(i=0;i<len;i++){
	str[i]=strtoadd[i];
}
str[len]=0;
}
//-----------------------------------------------------------------------------------------------------------
void memcopy(uint8 *str, const uint8 *strtoadd, uint16 len){
uint16 i;
for(i=0;i<len;i++){
	str[i]=strtoadd[i];
}
}
/*-----------------------------------------------------------------------------------*/
uint8 memcompare( uint8 *source, uint8 *reference, unsigned short len){
while(len){
     len--;
     if(source[len]!=reference[len]) return len+1;
}
return 0;
  
}

//-----------------------------------------------------------------------------------------------------------
uint16 longtotxt(uint8 *str, int32 no){
  uint8 i,t,j;
  i=0;
  j=0;
  if(no<0){
      *str=ISO_MINUS;
      j=1;
      no=no*(-1);
  }
  t = ISO_0 + no / 1000000000;
  if(t != ISO_0 ) {
    str[i+j] = t;
    i++;
  }  
  t = ISO_0 + (no / 100000000) % 10;
  if(t != ISO_0 || i!=0) {
    str[i+j] = t;
    i++;
  }
  t = ISO_0 + (no / 10000000) % 10;
  if(t != ISO_0 || i!=0) {
    str[i+j] = t;
    i++;
  }
  t = ISO_0 + (no / 1000000) % 10;
  if(t != ISO_0 || i!=0) {
    str[i+j] = t;
    i++;
  }
  t = ISO_0 + (no / 100000) % 10;
  if(t != ISO_0 || i!=0) {
    str[i+j] = t;
    i++;
  }
  t = ISO_0 + (no / 10000) % 10;
  if(t != ISO_0 || i!=0) {
    str[i+j] = t;
    i++;
  }
  t = ISO_0 + (no / 1000) % 10;
  if(t != ISO_0 || i!=0) {
    str[i+j] = t;
    i++;
  }
  t = ISO_0 + (no / 100) % 10;
  if(t != ISO_0 || i!=0) {
    str[i+j] = t;
    i++;
  }
  t = ISO_0 + (no / 10) % 10;
  if(t != ISO_0 || i!=0) {
    str[i+j] = t;
    i++;
  }
  str[i+j] = ISO_0 + (no % 10);
  i++;
  str[i+j]=0;
  return i+j;
}

//-----------------------------------------------------------------------------------------------------------
uint16 noPrefHexStrtoInt(uint8 *buffer,int16 *no){
uint16 len,t,s;
len=0;

s=1;
t=0;
while(s){
  s=0;
  if(buffer[len]>=ISO_0 && buffer[len]<=ISO_9){
    t*=16;
    t+=(buffer[len]-ISO_0);
    len++;
    s++;
  }
  if(buffer[len]>=ISO_A && buffer[len]<=ISO_F){
    t*=16;
    t+=(buffer[len]-ISO_A+10);
    len++;
    s++;
   }
   if(buffer[len]>=ISO_a && buffer[len]<=ISO_f){
    t*=16;
    t+=(buffer[len]-ISO_a+10);
    len++;
    s++;
   }
}
*no=t;
return len;
}
//-----------------------------------------------------------------------------------------------------------
uint16 noPrefHexStrtoUlong(uint8 *buffer,uint32 *no){
uint16 len,s;
uint32 t;
len=0;

s=1;
t=0;
while(s){
  s=0;
  if(buffer[len]>=ISO_0 && buffer[len]<=ISO_9){
    t*=16;
    t+=(buffer[len]-ISO_0);
    len++;
    s++;
  }
  if(buffer[len]>=ISO_A && buffer[len]<=ISO_F){
    t*=16;
    t+=(buffer[len]-ISO_A+10);
    len++;
    s++;
   }
   if(buffer[len]>=ISO_a && buffer[len]<=ISO_f){
    t*=16;
    t+=(buffer[len]-ISO_a+10);
    len++;
    s++;
   }
}
*no=t;
return len;
}
//-----------------------------------------------------------------------------------------------------------
void longtohexstr(uint8 *buffer,uint32 no){
uint16 i;
uint32 t;
buffer[0]=ISO_0;
buffer[1]=ISO_x;
for(i=0;i<8;i++){
  t=no&(0xF0000000>>(i*4));
  t=t>>(28-4*i);
  if(t<10){
        buffer[i+2]=t+ISO_0;
  }else buffer[i+2]=t-10+'A';
  }
  buffer[10]=0;
}
//-----------------------------------------------------------------------------------------------------------
uint16 noPrefHexStrtoChar(uint8 *buffer,int8 *no){
uint16 len,t,s;
len=0;
s=1;
t=0;
while(s){
  s=0;
  if(buffer[len]>=ISO_0 && buffer[len]<=ISO_9){
    t*=16;
    t+=(buffer[len]-ISO_0);
    len++;
    s++;
  } else if(buffer[len]>=ISO_A && buffer[len]<=ISO_F){
    t*=16;
    t+=(buffer[len]-ISO_A+10);
    len++;
    s++;
   } else if(buffer[len]>=ISO_a && buffer[len]<=ISO_f){
    t*=16;
    t+=(buffer[len]-ISO_a+10);
    len++;
    s++;
   };
}
*no=t;
return len;
}
