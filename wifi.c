/*
 * UART2_cli.c
 *
 *  Created on: Sep 8, 2016
 *      Author: Andrei
 */


#include "wifi.h"
#include "util.h"
#include "sysinit.h"
#include "mcg.h"
#include "version.h"
#include "timers.h"
#include "cli.h"
#include "IO_Process.h"

uint8 WifiTmpPrintBuff[64];

void WifiParser(vuint8 *CmdBuffer);

//size used as AND mask
#define UART2_RX_BUFF_SIZE 0x7FF
#define UART2_TX_BUFF_SIZE 0x7FF


Uart_Variables_type Uart2CLI;
vuint8 Uart2_TXBuffer[UART2_TX_BUFF_SIZE+1],Uart2_RXBuffer0[UART2_RX_BUFF_SIZE+1],Uart2_RXBuffer1[UART2_RX_BUFF_SIZE+1];

uint8 Uart2_CLI_Process_State =0;

uint8 ATprocessState;
#define AT_PROCESS_IDLE                0
#define AT_PROCESS_EXIT_SLEEP          1
#define AT_PROCESS_TRANSMITING         2
#define AT_PROCESS_ANS_RECEIVED        3
#define AT_PROCESS_WAIT_PROCESS_ANSWER 4

#define MAX_AT_MSG_NO 10
AT_msg_type ATmsgBuffer[MAX_AT_MSG_NO];
#define AT_MSG_EMPTY   0
#define AT_MSG_NEW     1
#define AT_MSG_SENDING 2

uint8 TCPdataBuffer[UART2_RX_BUFF_SIZE];

uint8 ATbufferWriteIndex, ATbufferReadIndex;
uint16 ATprocessTimer;

uint32 ProcessATCmdTimer;
#define AT_CMD_PROCESS_EXPIRE_SHORT (3*3+5)
#define AT_CMD_PROCESS_EXPIRE_LONG  (3*180+5)

#define AT_CMD_MAX_RETRIES 3
#define AT_NACK_TIMEOUT 20
uint8 ATcmdTimeout;
uint16 ATprocessTimer;

void ESP8266MsgParser(uint8* buffer,uint16 msg_len);

uint16 UploadDataLength;


#define MNG_INIT       0
#define MNG_CONF       1
#define MNG_NORMAL_OP  2
#define MNG_DISCONNECT 3
#define MNG_SEND_REQ   4
uint8 ManagementState = MNG_INIT;
uint16 ManagementFlags = 0;
#define MNG_FLAGS_CONFIGURED        0x0001
#define MNG_FLAGS_AT_CMD_PROC_END   0x0002
#define MNG_FLAGS_AT_TIMEOUT        0x0004
#define MNG_FLAGS_WIFI_CONNECTED    0x0008
#define MNG_FLAGS_SETTINGS_RECEIVED 0x0010
#define MNG_FLAGS_INIT_DONE         0x0020
#define MNG_FLAGS_WAIT_RST          0x0040
#define MNG_FLAGS_RESET_DONE        0x0080
#define MNG_SURPRISE_RESET          0x0100
#define MNG_FIRST_TRY               0x0200
#define MNG_LED_ON                  0x0400

uint8 ESP_03_Init(void);
uint8 InitIndex = 0;

uint8 ESP_03_Configure(void);
uint8 ConfigureIndex = 0;

uint8 ESP_03_Connect(void);
uint8 ConnectIndex = 0;

uint8 ESP_03_Disconnect(void);
uint8 DisconnectIndex = 0;

uint8 ESP_03_SendRequest(void);
uint8 RequestIndex = 0;


//const uint8 HttpResponse[] = {"POST /cart/product HTTP/1.1\r\nHost: 46.101.150.207\r\nContent-Length: 191\r\nConnection: keep-alive\r\n\r\ndelivery_user_adress_id=8987037&password=testmobile&wifi_name=It%27s+finger+lickin%27+good&part_number_key=test&wifi_pass=&invoice_type=PF&email=testmobile%40yahoo.com&payment_method=numerar"};
const uint8 HttpHeader[] = {"POST /send-order HTTP/1.1\r\nHost: 46.101.150.207\r\nConnection: keep-alive\r\nContent-Length: "};//191\r\nConnection: keep-alive\r\n\r\ndelivery_user_adress_id=8987037&password=testmobile&wifi_name=It%27s+finger+lickin%27+good&part_number_key=test&wifi_pass=&invoice_type=PF&email=testmobile%40yahoo.com&payment_method=numerar"};
//const uint8 HttpHeader2[] = {"\r\n\r\ndelivery_user_adress_id=8987037&password=testmobile&wifi_name=It%27s+finger+lickin%27+good&part_number_key=test&wifi_pass=&invoice_type=PF&email=testmobile%40yahoo.com&payment_method=numerar"};

uint8 SSID[64];
uint8 Pass[64];
uint8 ButtonCall[512];
uint16 ButtonCallLength;

uint8 TCPid = 0;

uint16 LedOnTimmer;
uint32 OrderSentTimmer;
/**********************************************************************************
Function Name: UART2 CLi init
Description:
Parameters:   None
Return value:   None
 ***********************************************************************************/
void Uart2CLIinit(void){
uint32 aux32=0;
Uart2CLI.TXBuffer = (uint8*)Uart2_TXBuffer;
Uart2CLI.RXBuffer = (uint8*)Uart2_RXBuffer0;
Uart2CLI.TXBuffSize = UART2_TX_BUFF_SIZE;
Uart2CLI.RXBuffSize = UART2_RX_BUFF_SIZE;
Uart2CLI.TXCharCount = 0;
Uart2CLI.TXWriteIndex = 0;
Uart2CLI.TXReadIndex = 0;
Uart2CLI.RXWriteIndex = 0;
Uart2CLI.uartChannel = UART2_BASE_PTR;
Uart2CLI.flags = BUFFEREMPTY;
uart_init ((UART_MemMapPtr)UART2_BASE_PTR,BusFlashClock,ESP_03_BAUD_RATE);
Uart2_CLI_Process_State = CLI_FREE;
}
/**********************************************************************************
Function Name: UART2_CLI_Process
Description:
Parameters:   None
Return value:   None
 ***********************************************************************************/
void UART2_CLI_Process(void){
uint16 msgLen;
uint8* msg;
if(Uart2CLI.flags & RX_TIMER_STARTED){
  if(TimerExpired(&Uart2CLI.RXtimer,RECEIVE_TIMEOUT)){
    Uart2_CLI_Process_State = CLI_NEW_DATA;
    Uart2CLI.flags &=~RX_TIMER_STARTED;
  }
}
switch(Uart2_CLI_Process_State){
  case CLI_FREE:
    break;
  case CLI_NEW_DATA:
    //change the receive buffer
    if(Uart2CLI.RXBuffer == Uart2_RXBuffer0){
      msg = (uint8*)Uart2_RXBuffer0;
      Uart2CLI.RXBuffer = Uart2_RXBuffer1;
    }else{
      msg = (uint8*)Uart2_RXBuffer1;
      Uart2CLI.RXBuffer = Uart2_RXBuffer0;
    }
    msgLen = Uart2CLI.RXWriteIndex;
    Uart2CLI.RXWriteIndex = 0;
    msg[msgLen]=0;
    ESP8266MsgParser(msg,msgLen);
    Uart2_CLI_Process_State = CLI_FREE;
    break;
  default:
    Uart2_CLI_Process_State = CLI_FREE;
    break;
}
}
/**********************************************************************************
Function Name: WifiTXData
Description:
Parameters:   None
Return value:   None
 ***********************************************************************************/
void WifiTXData(uint8 *data,uint16 len){
  SendSerialData(&Uart2CLI,data,len);
}
/**********************************************************************************
Function Name: WifiTXStr
Description:
Parameters:   None
Return value:   None
 ***********************************************************************************/
void WifiTXStr(uint8 *string){
/*
  RS232TXStr((uint8*)"\r\n ESP-03 sent:");
  RS232TXStr(string);
*/
  SendSerialStr(&Uart2CLI,string);
}
/**********************************************************************************
Function Name: SCI5_RX_TX_Irq
Description:
Parameters:   None
Return value:   None
 ***********************************************************************************/
void UART2_IRQHandler(void){
uint8 c;
//-------------------------------------TX int------------------------------
if((UART2_S1 & UART_S1_TDRE_MASK)&&(UART2_C2 & UART_C2_TIE_MASK)){
  //the tx int is enabled and the flag is asserted
  if (Uart2CLI.TXCharCount){                                   // send if chars are in buffer
    UART2_D =  Uart2CLI.TXBuffer [Uart2CLI.TXReadIndex++];// load tx register, inc index
    Uart2CLI.TXReadIndex = Uart2CLI.TXReadIndex & Uart2CLI.TXBuffSize;     // adjust index
    Uart2CLI.TXCharCount--;                                    // char sent, dec count
  } else {                                              // buffer empty, nothing to do
    Uart2CLI.flags |=BUFFEREMPTY;                          // set empty flag
    UART2_C2 &= ~UART_C2_TIE_MASK;// Transmit Interrupt Disable (TIE = 0 )
  }
  return;
}
if((UART2_S1 & UART_S1_RDRF_MASK)&&(UART2_C2 & UART_C2_RIE_MASK)){//the rx int is enabled and the flag is asserted
  //normal operation
  if(Uart2CLI.RXWriteIndex<(Uart2CLI.RXBuffSize-1)){
    Uart2CLI.RXBuffer[Uart2CLI.RXWriteIndex++] = UART2_D;
  }else{
    c =  UART2_D;
  }
  StartTimer(&Uart2CLI.RXtimer);
  Uart2CLI.flags |= RX_TIMER_STARTED;
}
}

/**********************************************************************************
Function Name: WifiTransmitDone
Description:
Parameters:   None
Return value:   1 -> no more data to transmit; 0 : tehre is still data in the buffer
 ***********************************************************************************/
uint8 WifiTransmitDone(void){
return (Uart2CLI.flags & BUFFEREMPTY);
}

/**********************************************************************************
Function Name:  gsmMsgParser
Description:
Parameters:     None
Return value:   None
***********************************************************************************/
void ESP8266MsgParser(uint8* buffer,uint16 msg_len){
uint16 len,cmd_len;
uint16 i,aux16;
uint8 date[7];
uint8 time[7];
uint8 c;
int32 ts;

if(msg_len<4) return;

/*
  RS232TXStr((uint8*)"\r\n#GSM_raw:");
  for(i=0;i<msg_len;i++){
    chartohexstr(TmpPrintBuff,buffer[i]);
    RS232TXStr(&TmpPrintBuff[2]);
    RS232TXStr((uint8*)" ");
  }
*/
RS232TXStr((uint8*)"\r\n\r\n#ESP-03:");
RS232TXData((uint8*)buffer,msg_len);
RS232TXStr((uint8*)"\r\n%>");


cmd_len = 0;

while(cmd_len <= msg_len){

  while(((buffer[cmd_len]==0x0D) || (buffer[cmd_len]==0x0A)) && (cmd_len <= msg_len)){
    cmd_len++;
  }

  if(cmd_len >= msg_len) break;
  //parse the received data
  if(buffer[cmd_len]=='>'){
    //the mc60 module is ready to output data (tcp/udp)
    WifiTXData((uint8*)TCPdataBuffer,UploadDataLength);//send tcp data
  }else if((len=strcomparewithoutend((uint8 *)&buffer[cmd_len],(uint8*)"ERROR"))){
    cmd_len+=len;
    if(ATprocessState == AT_PROCESS_TRANSMITING) ATprocessState = AT_PROCESS_ANS_RECEIVED;
  }else if((len=strcomparewithoutend((uint8 *)&buffer[cmd_len],(uint8*)"OK"))){
    cmd_len+=len;
    if(ATprocessState == AT_PROCESS_TRANSMITING) ATprocessState = AT_PROCESS_ANS_RECEIVED;
  }else if((len=strcomparewithoutend((uint8 *)&buffer[cmd_len],(uint8*)"Send OK"))){
    cmd_len+=len;
    if(ATprocessState == AT_PROCESS_TRANSMITING) ATprocessState = AT_PROCESS_ANS_RECEIVED;
  }else if((len=strcomparewithoutend((uint8 *)&buffer[cmd_len],(uint8*)"no change"))){
    cmd_len+=len;
    if(ATprocessState == AT_PROCESS_TRANSMITING) ATprocessState = AT_PROCESS_ANS_RECEIVED;
  }else if((len=strcomparewithoutend((uint8 *)&buffer[cmd_len],(uint8*)"ALREAY CONNECT"))){
    cmd_len+=len;
    if(ATprocessState == AT_PROCESS_TRANSMITING) ATprocessState = AT_PROCESS_ANS_RECEIVED;
  }else if((len=strcomparewithoutend((uint8 *)&buffer[cmd_len],(uint8*)"we must restart"))){
    cmd_len+=len;
    if(ATprocessState == AT_PROCESS_TRANSMITING) ATprocessState = AT_PROCESS_ANS_RECEIVED;
  }else if((len=strcomparewithoutend((uint8 *)&buffer[cmd_len],(uint8*)"FAIL"))){
    cmd_len+=len;
    if(ATprocessState == AT_PROCESS_TRANSMITING) ATprocessState = AT_PROCESS_ANS_RECEIVED;
  }else if((len=strcomparewithoutend((uint8 *)&buffer[cmd_len],(uint8*)"[System Ready"))){
    cmd_len+=len;
    if(ManagementFlags & MNG_FLAGS_WAIT_RST){
      ManagementFlags &=~MNG_FLAGS_WAIT_RST;
      ManagementFlags |= MNG_FLAGS_RESET_DONE;
    }else{
      if((ManagementFlags & MNG_FIRST_TRY)==0){
        ManagementFlags |= MNG_FIRST_TRY;
      }else{
        ManagementFlags |= MNG_SURPRISE_RESET;
      }
    }
  }else if((len=strcomparewithoutend((uint8 *)&buffer[cmd_len],(uint8*)"Content-Length:"))){
    cmd_len+=len;
    ManagementFlags |= MNG_FLAGS_SETTINGS_RECEIVED;
    while(buffer[cmd_len]==ISO_SPACE) cmd_len++;
    ButtonCallLength = 0;
    cmd_len+=strtoint(&buffer[cmd_len],&ButtonCallLength);//payload len
    //skip to the payload
    while(((buffer[cmd_len]==0x0D) || (buffer[cmd_len]==0x0A)) && (cmd_len <= msg_len)){
      cmd_len++;
    }
    for(i=0;i<ButtonCallLength;i++){
      ButtonCall[i] = buffer[cmd_len+i];
    }
    aux16 = 0;
    while(aux16<ButtonCallLength){
      if(len=strcomparewithoutend((uint8 *)&buffer[cmd_len+aux16],(uint8*)"wifi_name=")){
        aux16+=len;
        len = 0;
        while(buffer[cmd_len+aux16]!='&'){
          SSID[len++] = buffer[cmd_len+aux16++];
        }
        SSID[len]=0;
      }else if(len=strcomparewithoutend((uint8 *)&buffer[cmd_len+aux16],(uint8*)"wifi_pass=")){
        aux16+=len;
        len = 0;
        while(buffer[cmd_len+aux16]!='&'){
          Pass[len++] = buffer[cmd_len+aux16++];
        }
        Pass[len]=0;
      }
      while(buffer[cmd_len+aux16]!='&')aux16++;
      aux16++;//skip the &
    }
  }
  //jump to the next line
  while((buffer[cmd_len]!=0x0D) && (buffer[cmd_len]!=0x0A) && (cmd_len <= msg_len)){
    cmd_len++;
  }
}
}

/**********************************************************************************
Function Name:  ATCommandInit
Description:
Parameters:     None
Return value:   None
***********************************************************************************/
void ATCommandInit(void){
uint8 i;
//configure the AT MSG buffer
ATprocessState = AT_PROCESS_IDLE;
for(i=0;i<MAX_AT_MSG_NO;i++){
  ATmsgBuffer[i].state = AT_MSG_EMPTY;
}
ATcmdTimeout = 0;
ATbufferWriteIndex = 0;
ATbufferReadIndex = 0;

}

/**********************************************************************************
Function Name:  gsmAtProcess
Description:    process for sending at commands to the GSM module
Parameters:     None
Return value:   None
***********************************************************************************/
void gsmAtProcess(void){
uint16 timeout;
uint16 maxRetry;
switch(ATprocessState){
  case AT_PROCESS_IDLE:
    if(ATmsgBuffer[ATbufferReadIndex].state == AT_MSG_NEW){
      WifiTXStr((uint8*)ATmsgBuffer[ATbufferReadIndex].data);
      ATmsgBuffer[ATbufferReadIndex].state = AT_MSG_SENDING;
      ATprocessState = AT_PROCESS_TRANSMITING;
      StartTimer(&ATprocessTimer);
    }
    break;
  case AT_PROCESS_TRANSMITING:

    if(ATmsgBuffer[ATbufferReadIndex].flags & AT_MSG_FLAGS_LONG_TIMEOUT){
      timeout = 0xFFFF;
      maxRetry = 10;
    }else{
      timeout = 2000;
      maxRetry = 10;
    }
    if(TimerExpired(&ATprocessTimer,timeout)){
      /*
      if(ATcmdTimeout < maxRetry){
        ATcmdTimeout++;
        ATmsgBuffer[ATbufferReadIndex].state = AT_MSG_NEW;
        ATprocessState = AT_PROCESS_IDLE;
      }else{
        //timeout
        */
        ATcmdTimeout = 0;
        ATmsgBuffer[ATbufferReadIndex].state = AT_MSG_EMPTY;
        ATprocessState = AT_PROCESS_IDLE;
        if(++ATbufferReadIndex == MAX_AT_MSG_NO) ATbufferReadIndex = 0;
        ManagementFlags |= MNG_FLAGS_AT_CMD_PROC_END | MNG_FLAGS_AT_TIMEOUT;
      //}
    }

    break;
  case AT_PROCESS_ANS_RECEIVED:
    ATcmdTimeout = 0;
    ATmsgBuffer[ATbufferReadIndex].state = AT_MSG_EMPTY;
    ATprocessState = AT_PROCESS_IDLE;
    if(++ATbufferReadIndex == MAX_AT_MSG_NO) ATbufferReadIndex = 0;
    ManagementFlags |= MNG_FLAGS_AT_CMD_PROC_END;
    break;
  case AT_PROCESS_WAIT_PROCESS_ANSWER:
    break;
  default:
    ATprocessState = AT_PROCESS_IDLE;
}
}
/**********************************************************************************
Function Name:  gsmTransmitIdle
Description:
Parameters:     None
Return value:   None
***********************************************************************************/
uint8 WiFiTransmitIdle(void){
if(ATprocessState == AT_PROCESS_IDLE) return 1;
else return 0;
}
/**********************************************************************************
Function Name:  newAtCmd
Description:    adds a new at command in the msg buffer
Parameters:     None
Return value:   None
***********************************************************************************/
uint8 newAtCmd(uint8 *cmd,uint8 longTimeout){
uint8 len,res;
if(ATmsgBuffer[ATbufferWriteIndex].state == AT_MSG_EMPTY){
  //the buffer is not full yet
  len=0;
  while(*cmd!=0){
    ATmsgBuffer[ATbufferWriteIndex].data[len++] = *cmd++;
  }
  ATmsgBuffer[ATbufferWriteIndex].data[len++] = ISO_CR;
  ATmsgBuffer[ATbufferWriteIndex].data[len++] = ISO_NL;
  ATmsgBuffer[ATbufferWriteIndex].data[len++] = 0;
  ATmsgBuffer[ATbufferWriteIndex].state = AT_MSG_NEW;
  ATmsgBuffer[ATbufferWriteIndex].flags = 0;
  if(longTimeout) ATmsgBuffer[ATbufferWriteIndex].flags |= AT_MSG_FLAGS_LONG_TIMEOUT;
  ATbufferWriteIndex++;
  if(ATbufferWriteIndex==MAX_AT_MSG_NO) ATbufferWriteIndex = 0;
  ManagementFlags &=~(MNG_FLAGS_AT_CMD_PROC_END | MNG_FLAGS_AT_TIMEOUT);
}else{
  return 0;
}
return 1;
}

/**********************************************************************************
Function Name:  ManagementProcess
Description:
Parameters:     None
Return value:   None
***********************************************************************************/
void ManagementProcess(void){
//return;
switch(ManagementState){
  case MNG_INIT:
    if((ManagementFlags & MNG_FLAGS_INIT_DONE)==0){
      if(ESP_03_Init()){
        ManagementFlags |= MNG_FLAGS_INIT_DONE;
      }
    }else{
      if(ManagementFlags & MNG_FLAGS_CONFIGURED){
        ManagementState = MNG_NORMAL_OP;
        ConnectIndex = 0;
      }else{
        if(PB_Flags & PB_FLAGS_LONG_PRESS){
          PB_Flags &=~PB_FLAGS_LONG_PRESS;
          ManagementState = MNG_CONF;
          ConfigureIndex = 0;
        }
      }
    }
    break;
  case MNG_CONF:
    if(ESP_03_Configure()){
      ManagementFlags |= MNG_FLAGS_CONFIGURED;
      ManagementState = MNG_INIT;
      ManagementFlags &=~MNG_FLAGS_INIT_DONE;
      InitIndex=0;
      PB_Flags &=~PB_BUTTON_PRESSED;

    }
    break;
  case MNG_NORMAL_OP:
    if((ManagementFlags & MNG_FLAGS_WIFI_CONNECTED)==0){
      if(ESP_03_Connect()){
        ManagementFlags |= MNG_FLAGS_WIFI_CONNECTED;
      }
    }else{
      if(PB_Flags & PB_FLAGS_LONG_PRESS){
        PB_Flags &=~PB_FLAGS_LONG_PRESS;
        ManagementState = MNG_DISCONNECT;
        ManagementFlags &=~MNG_FLAGS_CONFIGURED;
        DisconnectIndex = 0;
      }else if(PB_Flags & PB_BUTTON_PRESSED){
        PB_Flags &=~PB_BUTTON_PRESSED;
        ManagementState = MNG_SEND_REQ;
        RequestIndex = 0;
      }else if(ManagementFlags & MNG_LED_ON){
        if(TimerExpired(&LedOnTimmer,3000)){
          ManagementFlags &=~MNG_LED_ON;
          New_Led_State = LED_OFF;
        }
      }



    }
    break;
  case MNG_DISCONNECT:
    if(ESP_03_Disconnect()){
      ManagementState = MNG_CONF;
      ConfigureIndex = 0;
    }
    break;
  case MNG_SEND_REQ:
    if(ESP_03_SendRequest()){
      ManagementState = MNG_NORMAL_OP;
      ConnectIndex = 0;
    }
    break;
  default:
    ManagementState = MNG_INIT;
    InitIndex = 0;
}
}
/**********************************************************************************
Function Name:  ESP_03_Init
Description:
Parameters:     None
Return value:   None
***********************************************************************************/
uint8 ESP_03_Init(void){
switch(InitIndex){
  case 0:
    //detect the module
    if(newAtCmd("AT",0)){
      InitIndex++;
    }
    break;
  case 1:
    //check that the command response was received
    if(ManagementFlags & MNG_FLAGS_AT_CMD_PROC_END){
      if(ManagementFlags & MNG_FLAGS_AT_TIMEOUT){
        InitIndex--;
      }else{
        InitIndex++;
      }
    }
    break;
  case 2:
    //enable echo
    if(newAtCmd("ATE1",0)){
      InitIndex++;
    }
    break;
  case 3:
    //check that the command response was received
    if(ManagementFlags & MNG_FLAGS_AT_CMD_PROC_END){
      if(ManagementFlags & MNG_FLAGS_AT_TIMEOUT){
        InitIndex--;
      }else{
        InitIndex++;
      }
    }
    break;
  case 4:
    //set wifi mode to generate ap
    if(newAtCmd("AT+CWMODE=3",0)){
      InitIndex++;
    }
    break;
  case 5:
    //check that the command response was received
    if(ManagementFlags & MNG_FLAGS_AT_CMD_PROC_END){
      if(ManagementFlags & MNG_FLAGS_AT_TIMEOUT){
        InitIndex--;
      }else{
        InitIndex++;
      }
    }
    break;
  case 6:
    RS232TXStr("\r\n ESP8266 Init Done!");
    InitIndex++;
    break;
  default:
    return 1;
}
return 0;
}
/**********************************************************************************
Function Name:  ESP_03_Configure
Description:
Parameters:     None
Return value:   None
***********************************************************************************/
uint8 ESP_03_Configure(void){
switch(ConfigureIndex){
  case 0:
    //generate ap
    if(newAtCmd("AT+CWSAP=\"Emag_Home\",\"Test1234\",7,3",1)){
      ConfigureIndex++;
      New_Led_State = LED_BLINK;
      ManagementFlags &=~MNG_LED_ON;
      ManagementFlags &=~MNG_FLAGS_SETTINGS_RECEIVED;
    }
    break;
  case 1:
    //check that the command response was received
    if(ManagementFlags & MNG_FLAGS_AT_CMD_PROC_END){
      if(ManagementFlags & MNG_FLAGS_AT_TIMEOUT){
        ConfigureIndex--;
      }else{
        ConfigureIndex++;
      }
    }
    break;
  case 2:
    //enable multiple connections
    if(newAtCmd("AT+CIPMUX=1",1)){
      ConfigureIndex++;
    }
    break;
  case 3:
    //check that the command response was received
    if(ManagementFlags & MNG_FLAGS_AT_CMD_PROC_END){
      if(ManagementFlags & MNG_FLAGS_AT_TIMEOUT){
        ConfigureIndex--;
      }else{
        ConfigureIndex++;
      }
    }
    break;
  case 4:
    //start a tcp server
    if(newAtCmd("AT+CIPSERVER=1,80",1)){
      ConfigureIndex++;
    }
    break;
  case 5:
    //check that the command response was received
    if(ManagementFlags & MNG_FLAGS_AT_CMD_PROC_END){
      if(ManagementFlags & MNG_FLAGS_AT_TIMEOUT){
        ConfigureIndex--;
      }else{
        ConfigureIndex++;
      }
    }
    break;
  case 6:
    if(ManagementFlags & MNG_FLAGS_SETTINGS_RECEIVED){
      ConfigureIndex++;
    }
    break;
  case 7:
    //stop the tcp server
    if(newAtCmd("AT+CIPCLOSE=0",1)){
      ConfigureIndex++;
    }
    break;
  case 8:
    //check that the command response was received
    if(ManagementFlags & MNG_FLAGS_AT_CMD_PROC_END){
      if(ManagementFlags & MNG_FLAGS_AT_TIMEOUT){
        ConfigureIndex--;
      }else{
        ConfigureIndex++;
      }
    }
    break;
  case 9:
    //stop the tcp server
    if(newAtCmd("AT+CIPSERVER=0",1)){
      ConfigureIndex++;
    }
    break;
  case 10:
    //check that the command response was received
    if(ManagementFlags & MNG_FLAGS_AT_CMD_PROC_END){
      if(ManagementFlags & MNG_FLAGS_AT_TIMEOUT){
        ConfigureIndex--;
      }else{
        ConfigureIndex++;
      }
    }
    break;
  case 11:
    //reset
    if(newAtCmd("AT+RST",1)){
      ConfigureIndex++;
      ManagementFlags |= MNG_FLAGS_WAIT_RST;
    }
    break;
  case 12:
    //check that the command response was received
    if(ManagementFlags & MNG_FLAGS_RESET_DONE){
      ManagementFlags &=~MNG_FLAGS_RESET_DONE;
      ConfigureIndex++;
    }
    break;
  default:
    RS232TXStr("\r\n ESP8266 Config Done!");
    New_Led_State = LED_ON;
    ManagementFlags &=~MNG_LED_ON;
    return 1;
}
return 0;
}

/**********************************************************************************
Function Name:  ESP_03_Configure
Description:
Parameters:     None
Return value:   None
***********************************************************************************/
uint8 ESP_03_Connect(void){
uint8 len = 0;
switch(ConnectIndex){
  case 0:
    //enable a single connection
    if(newAtCmd("AT+CIPMUX=0",1)){
      ConnectIndex++;
    }
    break;
  case 1:
    //check that the command response was received
    if(ManagementFlags & MNG_FLAGS_AT_CMD_PROC_END){
      if(ManagementFlags & MNG_FLAGS_AT_TIMEOUT){
        ConnectIndex--;
      }else{
        ConnectIndex++;
      }
    }
    break;
  case 2:
    //connect to wifi
    len = textadd(TmpPrintBuff,"AT+CWJAP=\"");
    len+= textadd(&TmpPrintBuff[len],SSID);
    len+= textadd(&TmpPrintBuff[len],"\",\"");
    len+= textadd(&TmpPrintBuff[len],Pass);
    len+= textadd(&TmpPrintBuff[len],"\"");
    if(newAtCmd(TmpPrintBuff,1)){
      ConnectIndex++;
    }
    break;
  case 3:
    //check that the command response was received
    if(ManagementFlags & MNG_FLAGS_AT_CMD_PROC_END){
      if(ManagementFlags & MNG_FLAGS_AT_TIMEOUT){
        ConnectIndex--;
      }else{
        ConnectIndex++;
      }
    }
    break;
  case 4:
    RS232TXStr("\r\n ESP8266 Connected to WIFI Done!");
    ConnectIndex++;
    break;
  default:
    return 1;
}
return 0;
}

/**********************************************************************************
Function Name:  ESP_03_Disconnect
Description:
Parameters:     None
Return value:   None
***********************************************************************************/
uint8 ESP_03_Disconnect(void){
switch(DisconnectIndex){
  case 0:
    //close connection
    if(newAtCmd("AT+CIPCLOSE",1)){
      DisconnectIndex++;
    }
    break;
  case 1:
    //check that the command response was received
    if(ManagementFlags & MNG_FLAGS_AT_CMD_PROC_END){
      if(ManagementFlags & MNG_FLAGS_AT_TIMEOUT){
        DisconnectIndex--;
      }else{
        DisconnectIndex++;
      }
    }
    break;
  default:
    return 1;
}
return 0;
}

/**********************************************************************************
Function Name:  ESP_03_SendRequest
Description:
Parameters:     None
Return value:   None
***********************************************************************************/
uint8 ESP_03_SendRequest(void){
switch(RequestIndex){
  case 0:
    if(ManagementFlags & MNG_SURPRISE_RESET){
      ManagementFlags &=~ MNG_SURPRISE_RESET;
      RequestIndex++;
      ConnectIndex=0;
    }else{
      RequestIndex+=2;
    }
    break;
  case 1:
    if(ESP_03_Connect()){
      RequestIndex++;
    }
    break;
  case 2:
    //connect to server
    if(newAtCmd("AT+CIPSTART=\"TCP\",\"46.101.150.207\",80",1)){
      RequestIndex++;
      New_Led_State = LED_BLINK;
      ManagementFlags &=~MNG_LED_ON;
    }
    break;
  case 3:
    //check that the command response was received
    if(ManagementFlags & MNG_FLAGS_AT_CMD_PROC_END){
      if(ManagementFlags & MNG_FLAGS_AT_TIMEOUT){
        RequestIndex--;
      }else{
        RequestIndex++;
      }
    }
    break;
  case 4:
    if(SendTCPdata()){
      RequestIndex++;
    }
    break;
  case 5:
    //check that the command response was received
    if(ManagementFlags & MNG_FLAGS_AT_CMD_PROC_END){
      if(ManagementFlags & MNG_FLAGS_AT_TIMEOUT){
        RequestIndex--;
      }else{
        RequestIndex++;
        New_Led_State = LED_ON;
        ManagementFlags |= MNG_LED_ON;
        StartTimer(&LedOnTimmer);
      }
    }
    break;
  default:
    return 1;
}
return 0;
}

/**********************************************************************************
Function Name:  ComposeTCPdata
Description:    send data
Parameters:     None
Return value:   None
***********************************************************************************/
uint8 SendTCPdata(void){
uint16 i,len=0,aux_len;
uint8 cmdBuff[20];
//compose the packet for sending

while(HttpHeader[len] !=0){
  TCPdataBuffer[len] = HttpHeader[len++];
}

aux_len=textadd(TmpPrintBuff,"\r\n\r\n");

len+=inttotxt(&TCPdataBuffer[len],ButtonCallLength);
len+=textadd(&TCPdataBuffer[len],TmpPrintBuff);
for(i=0;i<ButtonCallLength;i++){
  TCPdataBuffer[len++] = ButtonCall[i];
}

UploadDataLength = len;

aux_len = textadd(cmdBuff,(uint8*)"AT+CIPSEND=");
aux_len+= inttotxt(&cmdBuff[aux_len],len);
UploadDataLength = len;
if(newAtCmd((uint8*)cmdBuff,1)){
 return 1;
}
return 0;
}
