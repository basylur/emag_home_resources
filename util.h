#ifndef __UTIL_H__
#define __UTIL_H__

#include "ascii.h"
#include "types.h"

uint16 strtoint(uint8 *str, int16 *val);
uint16 strtofloat(uint8 *number,float *out);
uint16 strtolong(uint8 *str,int32 *val);
void inttohexstr(uint8 *buffer,uint16 no);
void chartohexstr(uint8 *buffer,uint8 no);
uint16 inttosizestr(uint8 *buffer,uint16 no,uint16 size);
uint16 hexstrtoint(uint8 *buffer,int16 *no);
uint16 inttotxt(uint8 *str, int16 no);
uint16 longtotxt(uint8 *str, int32 no);
uint8 textadd(uint8 *str, const uint8 *strtoadd);
void addlentext(uint8 *str, const uint8 *strtoadd, uint16 len);
uint8 strcompare( uint8 *text,uint8 *command);
uint16 strcomparewithoutend( uint8 *text, const uint8 *command);
uint16 noPrefHexStrtoInt(uint8 *buffer,int16 *no);
uint16 noPrefHexStrtoUlong(uint8 *buffer,uint32 *no);
uint16 noPrefHexStrtoChar(uint8 *buffer,int8 *no);
void longtohexstr(uint8 *buffer,uint32 no);
uint16 strlength( const uint8 *text);
void memcopy(uint8 *str, const uint8 *strtoadd, uint16 len);
uint8 memcompare( uint8 *source, uint8 *reference, uint16 len);
uint16 hexstrtolong(uint8 *buffer,int32 *no);
#endif
