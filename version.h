/*
 * version.h
 *
 *  Created on: Sep 8, 2016
 *      Author: Andrei
 */

#ifndef SOURCES_VERSION_H_
#define SOURCES_VERSION_H_

#define HARDWARE_REV 1

#define MAJOR_REV 1
#define MINOR_REV 0
#define BUILD_NO  1



/************************************************
Version 1.0 b1  08.09.2017

-initial build
******************************************************/



#endif /* SOURCES_APP_VERSION_H_ */
