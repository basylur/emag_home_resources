/*
 * mcg_lite.c
 *
 *  Created on: Sep 14, 2016
 *      Author: Andrei
 */

#include "MKL26Z4.h"
#include "mcg.h"


uint32 CoreClock,BusFlashClock,ms_10_at_busClock;


void ClockInit(void)
{

MCG_C2 = MCG_C2_IRCS_MASK;
  /*
//This assumes that the MCG is in default FEI mode out of reset.
//enable ext oscilator
//enable the external oscilator
OSC0_CR = OSC_CR_ERCLKEN_MASK;

// First move to FBE mode
// Enable external oscillator, RANGE=2, HGO=1, EREFS=1, LP=0, IRCS=0
MCG_C2 = MCG_C2_RANGE0(2) | MCG_C2_HGO0_MASK | MCG_C2_EREFS0_MASK;
// Select external oscilator and Reference Divider and clear IREFS to start ext osc
// CLKS=2, FRDIV=3, IREFS=0, IRCLKEN=0, IREFSTEN=0
MCG_C1 = MCG_C1_CLKS(2) | MCG_C1_FRDIV(3);

while (!(MCG_S & MCG_S_OSCINIT0_MASK)){};  // wait for oscillator to initialize
while ((MCG_S & MCG_S_IREFST_MASK)==MCG_S_IREFST_MASK){}; // wait for the reference to be switched to external clock
while (((MCG_S & MCG_S_CLKST_MASK) >> MCG_S_CLKST_SHIFT) != 0x2){}; // Wait for clock status bits to show clock source is ext ref clk

// Now in FBE
MCG_C5 = MCG_C5_PRDIV0(0x3);//divide by 4 the external clock of 8MHz = 2MHz
// Ensure MCG_C6 is at the reset default of 0. LOLIE disabled, PLL disabled, clk monitor disabled, PLL VCO divider is clear
MCG_C6 = 0x0;
// Select the PLL VCO divider and system clock dividers depending on clocking option
//outdiv1 =0 -> divide by 1: system/core clock = MCG = 48 MHz;
//outdiv4 =1 -> outdiv 1  * divide by 2: flash/bus clock = 24 Mhz
//SIM_CLKDIV1 = SIM_CLKDIV1_OUTDIV1(0) | SIM_CLKDIV1_OUTDIV4(1);
//update the internal variables
CoreClock = 48000000;//48 MHz
BusFlashClock = 24000000;//24 MHz


// Set the VCO divider and enable the PLL for 100MHz, LOLIE=0, PLLS=1, CME=0, VDIV=26
MCG_C6 = MCG_C6_PLLS_MASK | MCG_C6_VDIV0(0); //VDIV = 0 (x24)

while (!(MCG_S & MCG_S_PLLST_MASK)){}; // wait for PLL status bit to set

while (!(MCG_S & MCG_S_LOCK0_MASK)){}; // Wait for LOCK bit to set

// Now running PBE Mode

// Transition into PEE by setting CLKS to 0
// CLKS=0, FRDIV=3, IREFS=0, IRCLKEN=0, IREFSTEN=0
MCG_C1 &= ~MCG_C1_CLKS_MASK;

// Wait for clock status bits to update
while (((MCG_S & MCG_S_CLKST_MASK) >> MCG_S_CLKST_SHIFT) != 0x3){};

// Now running PEE Mode
 * */
CoreClock = 1280000;
BusFlashClock = 640000;
ms_10_at_busClock = 15625;
} //pll_init



