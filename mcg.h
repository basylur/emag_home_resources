/*
 * clocks.h
 *
 *  Created on: Sep 14, 2016
 *      Author: Andrei
 */

#ifndef SOURCES_MCG_H_
#define SOURCES_MCG_H_

#include "types.h"

void ClockInit (void);

extern uint32 CoreClock,BusFlashClock,ms_10_at_busClock;


#endif /* SOURCES_DRIVERS_MCG_LITE_CLOCKS_H_ */
