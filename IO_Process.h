/*
 * IO_Process.h
 *
 *  Created on: Oct 6, 2016
 *      Author: Andrei
 */

#ifndef SOURCES_IO_PROCESS_H_
#define SOURCES_IO_PROCESS_H_


#include "types.h"
#include "MKL26Z4.h"
#include "compileOptions.h"



#define BLUE_LED_SHIFT 5
#define BLUE_LED_ON()     do{GPIOD_PDDR |= ((uint32)1) << BLUE_LED_SHIFT; GPIOD_PCOR |= ((uint32)1) << BLUE_LED_SHIFT;}while(0)
#define BLUE_LED_OFF()    do{GPIOD_PDDR &= ~((uint32)1) << BLUE_LED_SHIFT;}while(0)


#define PB_SHIFT 1
#define PB_STATE   ((GPIOC_PDIR & (0x0001<<PB_SHIFT))>>PB_SHIFT)
#define PB_PRESSED  0
#define PB_RELEASED 1
#define PB_INIT     2


void PB_Process(void);

extern uint8 PB_Flags;
#define PB_FLAGS_LONG_PRESS 0x01
#define PB_BUTTON_PRESSED   0x02

extern uint8 LightOn;
#define DASH_LED_SHIFT 2
#define DASH_LED_ON()     do{ LightOn =1; GPIOC_PDDR |= ((uint32)1) << DASH_LED_SHIFT; GPIOD_PCOR |= ((uint32)1) << DASH_LED_SHIFT;}while(0)
#define DASH_LED_OFF()    do{ LightOn =0; GPIOC_PDDR &= ~((uint32)1) << DASH_LED_SHIFT;}while(0)

extern uint8 New_Led_State;
#define LED_OFF    0
#define LED_ON     1
#define LED_BLINK 2

void Dash_Led_Process(void);

#endif /* SOURCES_APP_IO_PROCESS_H_ */
