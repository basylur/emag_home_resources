#ifndef __ASCII_H__
#define __ASCII_H__

#define ISO_A 0x41
#define ISO_B 0x42
#define ISO_C 0x43
#define ISO_D 0x44
#define ISO_E 0x45
#define ISO_F 0x46
#define ISO_G 0x47
#define ISO_H 0x48
#define ISO_I 0x49
#define ISO_J 0x4A
#define ISO_K 0x4B
#define ISO_L 0x4C
#define ISO_M 0x4D
#define ISO_N 0x4E
#define ISO_O 0x4F
#define ISO_P 0x50
#define ISO_Q 0x51
#define ISO_R 0x52
#define ISO_S 0x53
#define ISO_T 0x54
#define ISO_U 0x55
#define ISO_V 0x56
#define ISO_W 0x57
#define ISO_X 0x58
#define ISO_Y 0x59
#define ISO_Z 0x5A


#define ISO_a 0x61
#define ISO_b 0x62
#define ISO_c 0x63
#define ISO_d 0x64
#define ISO_e 0x65
#define ISO_f 0x66
#define ISO_g 0x67
#define ISO_h 0x68
#define ISO_i 0x69
#define ISO_j 0x6a
#define ISO_k 0x6b
#define ISO_l 0x6c
#define ISO_m 0x6d
#define ISO_n 0x6e
#define ISO_o 0x6f
#define ISO_p 0x70
#define ISO_q 0x71
#define ISO_r 0x72
#define ISO_s 0x73
#define ISO_t 0x74
#define ISO_u 0x75
#define ISO_v 0x76
#define ISO_w 0x77
#define ISO_x 0x78
#define ISO_y 0x79

#define ISO_0 0x30
#define ISO_1 0x31
#define ISO_2 0x32
#define ISO_3 0x33
#define ISO_4 0x34
#define ISO_5 0x35
#define ISO_6 0x36
#define ISO_7 0x37
#define ISO_8 0x38
#define ISO_9 0x39


#define ISO_TAB         0x09
#define ISO_NL          0x0a
#define ISO_CR          0x0d
#define ISO_DOT         0x2e
#define ISO_SPACE       0x20
#define ISO_PLUS        0x2b
#define ISO_MINUS       0x2d
#define ISO_EQUAL       0x3d
#define ISO_BEEP        0x07
#define ISO_ESC         0x1B
#define ISO_BACKSLASH   0x2F
#define ISO_BACKSPACE   0x08
#define ISO_SLASH       0x2F
#define ISO_DEL         0x7F
#define ISO_COLON       0x3A
#define ISO_HASHTAG     0x23
#define ISO_CTRL_Z      0x1A

#endif
