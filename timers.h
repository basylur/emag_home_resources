/*
 * timers.h
 *
 *  Created on: Sep 12, 2016
 *      Author: Andrei
 */

#ifndef SOURCES_TIMERS_H_
#define SOURCES_TIMERS_H_

#include "types.h"

void PITInit(void);
void PIT_Change_Clock(void);
void PIT_IRQHandler(void);

void Delay_us(uint32 delay);
void StartTimer(uint16 *ptr);
uint8 TimerExpired(uint16 *timer, uint16 interval);

#define TIMEOUT_1S 100
#define TIMEOUT_1MIN 6000

extern vuint16 seconds,minutes,hours;
#endif /* SOURCES_DRIVERS_PIT_TIMERS_H_ */
