/*
 * powerModes.h
 *
 *  Created on: Sep 15, 2016
 *      Author: Andrei
 */

#ifndef SOURCES_DRIVERS_POWERMODES_H_
#define SOURCES_DRIVERS_POWERMODES_H_

void initPowerModes(void);
uint8 getPowerMode(void);

uint8 enter_vlpr(void);
uint8 exit_vlpr(void);
void enter_vlps(void);

#define POWER_MODE_RUN   0x01
#define POWER_MODE_VLPR  0x04
#define POWER_MODE_VLPW  0x08
#define POWER_MODE_LLS   0x20
#define REGULAR_SLEEP            0
#define SLEEP_WITH_MOTION_DETECT 1
void prepareForDeepSleep(uint8 option);
void returnFromDeepSleep(void);

#endif /* SOURCES_DRIVERS_POWERMODES_H_ */
