/*
 * IO_Process.c
 *
 *  Created on: Sep 10, 2017
 *      Author: Andrei
 */

#include "IO_Process.h"
#include "types.h"
#include "cli.h"
#include "timers.h"

/*
PB_SHIFT 1
#define PB_STATE   ((GPIOC_PDIR & (0x0001<<PB_SHIFT))>>PB_SHIFT)
#define PB_PRESSED  0
#define PB_RELEASED 1
*/
uint8 PB_Process_State = PB_RELEASED;
uint8 PB_Flags =0;
uint16 PB_Timer;
#define LONG_PRESS_TIMEOUT 1600

uint8 Dash_Led_Process_State = LED_OFF,New_Led_State = LED_OFF;
uint8 LightOn;
uint16 DashLedTimer;
#define ON_TIME    100
#define OFF_TIME   300


/**********************************************************************************
Function Name: RS232TXStr
Description:
Parameters:   None
Return value:   None
 ***********************************************************************************/
void PB_Process(void){
switch(PB_Process_State){
  case PB_INIT:
    if(TimerExpired(&PB_Timer,LONG_PRESS_TIMEOUT)){
      PB_Process_State = PB_RELEASED;
    }
    break;
  case PB_RELEASED:
    if(PB_STATE == PB_PRESSED){
      RS232TXStr("\r\n Push Button Pressed!");
      PB_Process_State = PB_PRESSED;
      StartTimer(&PB_Timer);
    }
    break;
  case PB_PRESSED:
    if(PB_STATE == PB_RELEASED){
      PB_Process_State = PB_RELEASED;
      BLUE_LED_OFF();
      PB_Flags |= PB_BUTTON_PRESSED;
    }
    if(TimerExpired(&PB_Timer,LONG_PRESS_TIMEOUT)){
      PB_Flags |= PB_FLAGS_LONG_PRESS;
      BLUE_LED_ON();
    }
    break;
  default:
    PB_Process_State = PB_RELEASED;
}
}


/**********************************************************************************
Function Name: Dash_Led_Process
Description:
Parameters:   None
Return value:   None
 ***********************************************************************************/
void Dash_Led_Process(void){
switch(Dash_Led_Process_State){
  case LED_OFF:
    if(New_Led_State!=LED_OFF){
      DASH_LED_ON();
      Dash_Led_Process_State = New_Led_State;
      StartTimer(&DashLedTimer);
    }
    break;
  case LED_ON:
    if(New_Led_State!=LED_ON){
      DASH_LED_OFF();
      Dash_Led_Process_State = New_Led_State;
      StartTimer(&DashLedTimer);
    }
    break;
  case LED_BLINK:
    if(New_Led_State == LED_OFF){
      DASH_LED_OFF();
      Dash_Led_Process_State = New_Led_State;
    }else if(New_Led_State == LED_ON){
      DASH_LED_ON();
      Dash_Led_Process_State = New_Led_State;
    }else{
      if(LightOn){
        if(TimerExpired(&DashLedTimer,ON_TIME)){
          DASH_LED_OFF();
        }
      }else{
        if(TimerExpired(&DashLedTimer,OFF_TIME)){
          DASH_LED_ON();
        }
      }
    }
    break;
  default:
    New_Led_State = LED_OFF;
}
}

