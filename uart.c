/*
 * File:        uart.c
 * Purpose:     Provide common UART routines for serial IO
 *
 * Notes:       
 *              
 */

#include "uart.h"
#include "ascii.h"
#include "sysinit.h"
#include "MKL26Z4.h"

/********************************************************************/
/*
 * Initialize the UART for 8N1 operation, interrupts enabled, and
 * no hardware flow-control
 *
 * NOTE: Since the UARTs are pinned out in multiple locations on most
 *       Kinetis devices, this driver does not enable UART pin functions.
 *       The desired pins should be enabled before calling this init function.
 *
 * Parameters:
 *  uartch      UART channel to initialize
 *  sysclk      UART module Clock in kHz(used to calculate baud)
 *  baud        UART baud rate
 */

void uart_init (UART_MemMapPtr uartch, uint32 uart_clk_Hz, uint32 baud){
  uint32 sbr;
  uint8 temp;

  if(uartch == (UART_MemMapPtr)UART0_BASE_PTR){
    SIM_SCGC4 |=  SIM_SCGC4_UART0_MASK;
    SIM_SOPT2 |= SIM_SOPT2_UART0SRC(1);//select mcgfllclk as the clock for uart0
  }else if(uartch == UART1_BASE_PTR){
    SIM_SCGC4 |=  SIM_SCGC4_UART1_MASK;
  }else if(uartch == UART2_BASE_PTR){
    SIM_SCGC4 |=  SIM_SCGC4_UART2_MASK;
  }
  // Make sure that the transmitter and receiver are disabled while we
  // change settings.
  //
  UART_C2_REG(uartch) &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK );

  // Configure the UART for 8-bit mode, no parity
  UART_C1_REG(uartch) = 0;	// We need all default settings, so entire register is cleared

   // Calculate baud settings
  sbr = uart_clk_Hz/baud;

  // Save off the current value of the UARTx_BDH except for the SBR field
  temp = UART_BDH_REG(uartch) & ~(UART_BDH_SBR(0x1F));
  UART_BDH_REG(uartch) = temp |  UART_BDH_SBR(((sbr & 0x1F00) >> 8));
  UART_BDL_REG(uartch) = (uint8)(sbr & UART_BDL_SBR_MASK);
  // Save off the current value of the UARTx_C4 register except for the BRFA field
  // Enable receiver and transmitter
  UART_C2_REG(uartch) |= (UART_C2_TE_MASK	| UART_C2_RE_MASK | UART_C2_RIE_MASK);

  //Enable int in NVIC
  if(uartch == (UART_MemMapPtr)UART0_BASE_PTR){
    NVIC_EnableIRQ(UART0_IRQn);
  }else if(uartch == UART1_BASE_PTR){
    NVIC_EnableIRQ(UART1_IRQn);
  }else if(uartch == UART2_BASE_PTR){
    NVIC_EnableIRQ(UART2_IRQn);
  }
}


/**********************************************************************************
Function Name: 	
Description:	
Return value: 	None
 ***********************************************************************************/
void SendSerialStr(Uart_Variables_type *uart,uint8 *string){
UART_C2_REG((UART_MemMapPtr)uart->uartChannel) &= ~(UART_C2_TIE_MASK);//disable interrupt
while(*string!=0){
	if(uart->TXCharCount < uart->TXBuffSize){
	  uart->TXBuffer[uart->TXWriteIndex++] = *string;           // load byte to buffer and inc index
	  uart->TXWriteIndex = uart->TXWriteIndex & uart->TXBuffSize;      // adjust index to borders of buffer
	  uart->TXCharCount++;                                  // new char, inc count
		string++;
	} else {
	  //xxx
	  //FeedIntWatchdog();
		UART_C2_REG((UART_MemMapPtr)uart->uartChannel) |= UART_C2_TIE_MASK;//enable interrupt
		while(uart->TXCharCount > (uart->TXBuffSize-10));
		UART_C2_REG((UART_MemMapPtr)uart->uartChannel) &= ~(UART_C2_TIE_MASK);//disable interrupt
	}
}
if (uart->flags & BUFFEREMPTY){                          // buffer had been empty
  uart->flags &=(~BUFFEREMPTY);
}
UART_C2_REG((UART_MemMapPtr)uart->uartChannel) |= UART_C2_TIE_MASK;//enable interrupt
}
/**********************************************************************************
Function Name:  
Description:  
Return value:   None
 ***********************************************************************************/
void SendSerialData(Uart_Variables_type *uart,uint8 *data,uint16 len){
uint16 c=0;
UART_C2_REG((UART_MemMapPtr)uart->uartChannel) &= ~(UART_C2_TIE_MASK);//disable interrupt
while(c < len){
  if(uart->TXCharCount < uart->TXBuffSize){
    uart->TXBuffer[uart->TXWriteIndex++] = data[c];           // load byte to buffer and inc index
    uart->TXWriteIndex = uart->TXWriteIndex & uart->TXBuffSize;      // adjust index to borders of buffer
    uart->TXCharCount++;                                  // new char, inc count
    c++;
  } else {
    UART_C2_REG((UART_MemMapPtr)uart->uartChannel) |= UART_C2_TIE_MASK;//enable interrupt
    while(uart->TXCharCount > (uart->TXBuffSize-10));
    UART_C2_REG((UART_MemMapPtr)uart->uartChannel) &= ~(UART_C2_TIE_MASK);//disable interrupt
  }
}
if (uart->flags &BUFFEREMPTY){                          // buffer had been empty
  uart->flags &=(~BUFFEREMPTY);
}
UART_C2_REG((UART_MemMapPtr)uart->uartChannel) |= UART_C2_TIE_MASK;//enable interrupt
}
/********************************************************************/
void Default_Uart_RX_TX_Irq(Uart_Variables_type *uart){
uint8 c;
//-------------------------------------TX int------------------------------ 
if((UART_S1_REG((UART_MemMapPtr)uart->uartChannel) & UART_S1_TDRE_MASK) && (UART_C2_REG((UART_MemMapPtr)uart->uartChannel) & UART_C2_TIE_MASK)){
//the tx int is enabled and the flag is asserted
  if (uart->TXCharCount){                                   // send if chars are in buffer
    UART_D_REG((UART_MemMapPtr)uart->uartChannel) = uart->TXBuffer[uart->TXReadIndex++];// load tx register, inc index
    uart->TXReadIndex = uart->TXReadIndex & uart->TXBuffSize;          // adjust index
    uart->TXCharCount--;                                    // char sent, dec count
  } else {                                              // buffer empty, nothing to do
    uart->flags |=BUFFEREMPTY;                          // set empty flag
    UART_C2_REG((UART_MemMapPtr)uart->uartChannel) &= ~UART_C2_TIE_MASK; // Transmit Interrupt Disable (TIE = 0 )
  }
  return;
}

//----------------------------------------RX int----------------------------
if((UART_S1_REG((UART_MemMapPtr)uart->uartChannel) & UART_S1_RDRF_MASK) && (UART_C2_REG((UART_MemMapPtr)uart->uartChannel) & UART_C2_RIE_MASK)){//the rx int is enabled and the flag is asserted
#if SEACORP_SHMC
  if((RS485_Flags & RS485_TIMER_STARTED)==0)RS485_Flags |= RS485_TIMER_STARTED;
  StartTimer(&RS485_ReceiveTimer); 
  //if there is space save the character 
  if(uart->RXWriteIndex<(uart->RXBuffSize-1)){
    uart->RXBuffer[uart->RXWriteIndex++]=UART_D_REG(uart->uartChannel);// get the char
  }else{
    c  = UART_D_REG(uart->uartChannel);//dump the char
  }
#else
  c = UART_D_REG((UART_MemMapPtr)uart->uartChannel);// get the char
  if((uart->flags & REPETLASTCOMMAND)!=0){
    c = 0;
  }
  if((uart->flags & NEWDATA)!=0){
    if((c==ISO_ESC)||(c==155)) uart->flags |=ESCAPECODE;
    return;
  }
  if (uart->flags & ESCAPECODE){
    if(!((c==ISO_ESC)||(c==155))){//if esc wasn't presed before arrow up 
      if (c!='[') uart->flags &= ~ESCAPECODE;
      if((c=='A') && (uart->RXWriteIndex==0) && (!(uart->flags & BACKSPACE))){
        uart->flags |= REPETLASTCOMMAND;
      }
      c=0;
    }
  }
  if((c==ISO_ESC)||(c==155)){
    uart->flags |=ESCAPECODE;
  } else if(c==ISO_CR){
    uart->flags |= NEWDATA;
    uart->flags &= ~BACKSPACE;
    uart->RXBuffer[uart->RXWriteIndex++]=0;
  } else if(c>31 && c<127 ){
    if(uart->RXWriteIndex<(uart->RXBuffSize-1)){
      //xxx send char back if required ?
      uart->RXBuffer[uart->RXWriteIndex++]=c;
    }
    //xxx if there is no more space send beep ?
  }
  //xxx if back space or del received  remove char from buffer
#endif
}
}
/**********************************************************************************
Function Name:  enableRXActiveEdgeInterrupt
Description:    enables the RX active edge interrupt for exiting VLPS
Return value:   None
 ***********************************************************************************/
void enableRXActiveEdgeInterrupt(Uart_Variables_type *uart){
UART_BDH_REG((UART_MemMapPtr)uart->uartChannel) |= UART_BDH_RXEDGIE_MASK;
}
/**********************************************************************************
Function Name:  disableRXActiveEdgeInterrupt
Description:    disables the RX active edge interrupt
Return value:   None
 ***********************************************************************************/
void disableRXActiveEdgeInterrupt(Uart_Variables_type *uart){
UART_BDH_REG((UART_MemMapPtr)uart->uartChannel) &= ~UART_BDH_RXEDGIE_MASK;
}
